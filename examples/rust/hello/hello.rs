/*

This is a comment

/*

Nested comments also seems to work

*/

*/

// This is also a comment and ends with this line

// Some of the obvious good things are
// Nested comments
// Semicolon is a separator rather than a terminator (In C it is a terminator and is a source of lots
// of bugs)
//
//
fn main ()
{
    println!("hello world");
    println!("goodbye world")
}
